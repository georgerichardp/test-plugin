import 'dart:developer';

import 'base_route.dart';

class AllRoute {
  static Map<String, BaseRoute> allRouteMap = {};

  void getKey(List<BaseRoute> allRoute) {
    if (allRouteMap.isNotEmpty) {
      return;
    }
    log("execute MAPPING");
    for (var e in allRoute) {
      allRouteMap[e.routeName] = e;
    }
    log("executed MAPPING");
  }
}
