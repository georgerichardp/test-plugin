import 'dart:convert';
import 'dart:developer';

import 'package:encrypt/encrypt.dart' as enc;

class ISTCrypto {
  String ivDefault = "0000000000000000";
  List<int> keyDefaultList = [
    110,
    104,
    100,
    89,
    119,
    122,
    110,
    105,
    76,
    57,
    69,
    104,
    101,
    101,
    90,
    79,
    52,
    118,
    72,
    82,
    55,
    56,
    118,
    101,
    68,
    105,
    70,
    118,
    57,
    74,
    74,
    80
  ];


  static final instance = ISTCrypto._internal();

  ISTCrypto._internal();

  factory ISTCrypto() {
    return instance;
  }

  Future<String> decryptAES(String body) async {
    return await decryptAESBody(body, utf8.decode(keyDefaultList));
  }

  Future<String> encryptAES(String body) async {
    return await encryptAESBody(body, utf8.decode(keyDefaultList));
  }

  Future<String> encryptAESBody(String body, String keyRaw) async {
    if (body.isEmpty) return "";
    try {
      final key = enc.Key.fromUtf8(keyRaw);
      final iv = enc.IV.fromUtf8(ivDefault);
      final encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.ecb));
      enc.Encrypted encrypted = encrypter.encrypt(body, iv: iv);
      return (encrypted.base64);
    } catch (e) {
      log(e.toString());
    }
    return body;
  }

  Future<String> decryptAESBody(String body, String keyRaw) async {
    if (body.isEmpty) return "";
    try {
      final key = enc.Key.fromUtf8(keyRaw);
      final iv = enc.IV.fromUtf8(ivDefault);
      final encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.ecb));
      String encrypted =
          encrypter.decrypt(enc.Encrypted(base64.decode(body)), iv: iv);
      return encrypted;
    } catch (e) {
      log(e.toString());
    }
    return "";
  }
}