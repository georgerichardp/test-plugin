import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

///extensions ini untuk memanipulasi variable yang memiliki type data string
///semua function dimulai dengan i (contoh iClear) kenapa harus gini karna
///takutnya di class String atau package lain ada function yang sama
///dan untuk pemanggilan lebih mudah juga
extension IStringExtension on String {
  void iCopyToClipboard(BuildContext context, {String? desc}) {
    Clipboard.setData(ClipboardData(text: this));
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(desc ?? "Text copied to clipboard"),
    ));
  }

  int currencyToInt() {
    int data = 0;
    try {
      if (endsWith(",00")) {
        var currency = replaceAll(",00", "").replaceAllMapped(
          RegExp(r'^([^,.]*[.,])|\D+'),
          (Match m) =>
              m[1] != null ? m[1]!.replaceAll(RegExp(r'[^0-9]+'), '') : '',
        );
        data = int.parse(currency);
      } else {
        var currency = replaceAllMapped(
          RegExp(r'^([^,.]*[.,])|\D+'),
          (Match m) =>
              m[1] != null ? m[1]!.replaceAll(RegExp(r'[^0-9]+'), '') : '',
        );
        data = int.parse(currency);
      }
    } catch (_) {}
    return data;
  }

  double currencyToDouble() {
    double data = 0.0;
    try {
      var currency = replaceAllMapped(
        RegExp(r'^([^,.]*[.,])|\D+'),
        (Match m) =>
            m[1] != null ? m[1]!.replaceAll(RegExp(r'[^0-9]+'), '') : '',
      );
      data = double.parse(currency);
    } catch (_) {}
    return data;
  }

  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }

  String getAlphabet() {
    String data = "";
    data = replaceAll(RegExp('[^a-zA-Z ]'), "");
    return data;
  }

  bool get iIsAlphabetAndSpace {
    bool data = false;
    data = !contains(RegExp('[^a-zA-Z ]'));
    return data;
  }

  bool get iIsNumberAndSpace {
    bool data = false;
    data = !contains(RegExp('[^0-9 ]'));
    return data;
  }


  Color? get iToColor {
    final buffer = StringBuffer();
    if (length == 6 || length == 7) buffer.write('ff');
    buffer.write(replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  bool get iTimeInRange {
    try {
      var listOffTime = split("-");
      if (listOffTime.length == 2) {
        var dateNow = DateTime.now();
        var dateStart = DateFormat("HH:mm").parse(listOffTime.first);
        var dateEnd = DateFormat("HH:mm").parse(listOffTime[1]);
        var dateTimeStart = DateTime(
          dateNow.year,
          dateNow.month,
          dateNow.day,
          dateStart.hour,
          dateStart.minute,
        );
        var dateTimeEnd = DateTime(
          dateNow.year,
          dateNow.month,
          dateNow.day,
          dateEnd.hour,
          dateEnd.minute,
        );
        if (DateTime.now().isAfter(dateTimeStart) &&
            DateTime.now().isBefore(dateTimeEnd)) {
          return true;
        } else {
          return false;
        }
      }
    } catch (e) {
      if (kDebugMode) {
        print('$e');
      }
      return false;
    }
    return false;
  }

  String get iInitial {
    var data = "";
    if (isEmpty) {
      return data;
    }
    var dataSplit = split(" ");
    data += dataSplit[0][0];
    if (dataSplit.length > 1) {
      data += dataSplit[1][0];
    }
    return data;
  }

  bool get emailValidation {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}
