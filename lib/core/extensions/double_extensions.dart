import 'package:intl/intl.dart';

///extensions ini untuk memanipulasi variable yang memiliki type data double
///semua function dimulai dengan i (contoh iClear) kenapa harus gini karna
///takutnya di class String atau package lain ada function yang sama
///dan untuk pemanggilan lebih mudah juga
extension IDoubleExtensions on double {
  String iToRp() {
    final currencyFormatter = NumberFormat.currency(
      locale: 'ID',
      name: 'Rp ',
      decimalDigits: 0,
    );
    String dataText = currencyFormatter.format(this);
    return dataText;
  }
}
