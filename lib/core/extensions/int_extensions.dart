import 'package:intl/intl.dart';

///extensions ini untuk memanipulasi variable yang memiliki type data int
///semua function dimulai dengan i (contoh iToString) kenapa harus gini karna
///takutnya di class String atau package lain ada function yang sama
///dan untuk pemanggilan lebih mudah juga
extension IIntExtensions on int{
  String iToRp(){
    final currencyFormatter = NumberFormat.currency(
      locale: 'ID',
      name: 'Rp ',
      decimalDigits: 0,
    );
    String dataText = currencyFormatter.format(this);
    return dataText;
  }

  String toCurrency({bool withSymbol = true}) {
    return NumberFormat.currency(
        locale: 'id', symbol: withSymbol ? "Rp" : "", decimalDigits: 0)
        .format(this);
  }
}