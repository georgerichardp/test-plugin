import 'package:flutter/material.dart';


extension ISizeExtensions on BuildContext {
  static const defaultPadding = 8;
  //=========== color ===================
  Color get primaryColor => Theme.of(this).primaryColor;

  Color get disableColor => Theme.of(this).disabledColor;

  Color get errorColor => Theme.of(this).errorColor;

  //=========== Typography =============
  TextStyle? get displayLarge => Theme.of(this).textTheme.displayLarge;

  TextStyle? get displayMedium => Theme.of(this).textTheme.displayMedium;

  TextStyle? get displaySmall => Theme.of(this).textTheme.displaySmall;

  TextStyle? get headlineLarge => Theme.of(this).textTheme.headlineLarge;

  TextStyle? get headlineMedium => Theme.of(this).textTheme.headlineMedium;

  TextStyle? get headlineSmall => Theme.of(this).textTheme.headlineSmall;

  TextStyle? get titleLarge => Theme.of(this).textTheme.titleLarge;

  TextStyle? get titleMedium => Theme.of(this).textTheme.titleMedium;

  TextStyle? get titleSmall => Theme.of(this).textTheme.titleSmall;

  TextStyle? get labelLarge => Theme.of(this).textTheme.labelLarge;

  TextStyle? get labelMedium => Theme.of(this).textTheme.labelMedium;

  TextStyle? get labelSmall => Theme.of(this).textTheme.labelSmall;

  TextStyle? get bodyLarge => Theme.of(this).textTheme.bodyLarge;

  TextStyle? get bodyMedium => Theme.of(this).textTheme.bodyMedium;

  TextStyle? get bodySmall => Theme.of(this).textTheme.bodySmall;

  //======= Size =========
  MediaQueryData get iMediaQuery => MediaQuery.of(this);

  Size get iSize => MediaQuery.of(this).size;

  //kenapa ini tidak di awali dengan i karna untuk mempermudah pemanggilan saja
  double get padding0 => defaultPadding / 2;

  double get padding1 => defaultPadding * 1;

  double get padding2 => defaultPadding * 2;

  double get padding3 => defaultPadding * 3;

  double get padding4 => defaultPadding * 4;

  double get padding5 => defaultPadding * 5;

  double get padding6 => defaultPadding * 6;

  double get padding7 => defaultPadding * 7;

  double get padding8 => defaultPadding * 8;

  double get padding9 => defaultPadding * 9;

  double get padding10 => defaultPadding * 10;

  double mQWidth(double number) => iSize.width * number;

  double mQHeight(double number) => iSize.height * number;

  ///ini merupakan SizeBox dengat height. [size] defaultnya adalah [padding2]
  Widget sbHeight({double? size}) {
    return SizedBox(height: size ?? padding2);
  }

  ///ini merupakan SizeBox dengat width. [size] defaultnya adalah [padding2]
  Widget sbWidth({double? size}) {
    return SizedBox(width: size ?? padding2);
  }
}
