import 'package:intl/intl.dart';

extension IDateTimeExtensions on DateTime{
  String iDateNumber(){
    return DateFormat("dd-MM-yyyy").format(this);
  }
  String iDateName(){
    return DateFormat("dd-MMM-yyyy").format(this);
  }
}