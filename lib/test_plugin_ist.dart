
import 'dart:async';

import 'package:flutter/services.dart';

class TestPluginIst {
  static const MethodChannel _channel = MethodChannel('test_plugin_ist');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
